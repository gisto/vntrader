/*
1.本文件为VNTrader 期货CTP交易库底层代码
2.VNTrader及本C++库开源协议GPLV3协议

对VNPY开源库做出贡献的，并得到原始作者肯定的，将公布在http://www.vnpy.cn网站上，
并添加在《开源说明和感谢.txt》，并将该文件不断更新放入每一个新版本的vnpy库里。

官方网站：http://www.vnpy.cn
*/
#include "stdafx.h"
#include "MdSpi.h"
#include "mapdef.h"
#include <map>
#include <string>
#include <algorithm>
#include "iostream"

#include <iostream> 
#include <windows.h>
using namespace std;

CThostFtdcMdApi *mpUserApi = NULL;
CMdSpi vnmdspi;

#pragma warning(disable : 4996)

extern std::string gFrontAddr[3];
extern std::string gBrokerID;
extern std::string gInvestorID;
extern std::string gPassword;

extern char* ppInstrumentID[];	
extern CThostFtdcDepthMarketDataField *depthdata[];
extern int size;
extern int amount;
//extern std::map<std::string, int> gMarket;

 
extern CThostFtdcDepthMarketDataField* depthdata1;
 
using namespace std;
typedef map<string, InstrumentInfo> QS_Strategy_Map;
typedef hash_map<string, MarketdataSource> QS_Data_Map;
extern QS_Data_Map     gMarket;
extern QS_Strategy_Map mapStrategy;
//新增加

 

int errornum_del = 0;
int errornum_add = 0;


bool NotFirst_3M = false;
bool NotFirst_5M = false;
bool NotFirst_10M = false;
bool NotFirst_15M = false;
bool NotFirst_30M = false;
bool NotFirst_60M = false;

double CAL_MIDDLE_AMOUNT = 80;
double CAL_BIG_AMOUNT = 50;// = { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 ,100, 100, 100, 100, 100, 100, 100, 100, 100, 100 };        //大品种大单的定义
bool CAL_AMOUNT_STATE = true;// = { true, true, true, true, true, true, true, true, true, true ,true, true, true, true, true, true, true, true, true, true };        //true需要更新BIG_AMOUNT
int  GetMin(double thistime)
{
	int h = (int)(thistime * 100); //  21
	int m = (int)(thistime * 10000) - 100 * h; //  2130-2100=30
	return  (h * 60 + m);
}
string dbtoch(double nums)		//将double 转换为 string 进行保存
{
	char chr[20] = { 0 };
	sprintf(chr, "%.0f", nums);
	string tt = chr;
	return tt;
}


void WirteSingleRecordToFile8(int id, const char * content)
{
	char str[200] = { 0 };
	strcat_s(str, 200, "TradeRecord.txt");
	ifstream inf;
	ofstream ouf;
	inf.open(str, ios::out);
	ofstream o_file(str, ios::app);
	o_file << content << "\t" << endl;
	o_file.close();		
}

inline bool  CheckData(double data)
{
	if ((!_isnan(data)) && data > 1e-10)
	{
		return true;
	}
	return false;
}
inline bool  CheckDataOnlyNum(double data)
{
	if (!_isnan(data))
	{
		return true;
	}
	return false;
}
double zero_max(double a, double b)
{
	if (a == 0 && b == 0)
	{
		return 0;
	}
	else if (a == 0)
	{
		return b;
	}
	else if (b == 0)
	{
		return a;
	}
	else
	{
		return max(a, b);
	}
}

double zero_min(double a, double b)
{
	if (a == 0 && b == 0)
	{
		return 0;
	}
	else if (a == 0)
	{
		return b;
	}
	else if (b == 0)
	{
		return a;
	}
	else
	{
		return min(a, b);
	}
}
bool CheckFirst(double time, int cycle)
{
	int temp = (int)(time * 10000) - 100 * int(time * 100);
	if (temp % cycle == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}



DWORD WINAPI QryThreadProc(void* p)	//更新排名
{
	return 1;
	while (true)
	{
		CThostFtdcDepthMarketDataField  obj  ;
		memset(&obj, 0, sizeof(CThostFtdcDepthMarketDataField));
		_snprintf_s(obj.InstrumentID, sizeof(TThostFtdcInstrumentIDType), sizeof(TThostFtdcInstrumentIDType) - 1, "%s", "rb2110");
		obj.LastPrice=3500.0;
 		vnmdspi.PMsg(nThreadID_OnRtnDepthMarketData, MY_OnRtnDepthMarketData, &obj, NULL, 0);
		Sleep(1000);
	}
}
void CMdSpi::PMsg(unsigned nThreadID, int msg, LPVOID p1, LPVOID p2, int Reason)
{
	switch (msg)
	{
	case MY_OnFrontConnected:
		if (!::PostThreadMessage(nThreadID, msg, 0, 0))
		{
			printf("post message(OnFrontConnected) failed, errno:%d\n", ::GetLastError());
		}
		break;
	case MY_OnFrontDisconnected:
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)Reason, NULL))
		{
			printf("post message(MY_OnFrontDisconnected) failed, errno:%d\n", ::GetLastError());
		}
		break;
	case MY_OnRspUserLogin:
	{
		CThostFtdcRspUserLoginField * t1 = new CThostFtdcRspUserLoginField;
		memset(t1, 0, sizeof(CThostFtdcRspUserLoginField));
		memcpy_s(t1, sizeof(CThostFtdcRspUserLoginField),p1,sizeof(CThostFtdcRspUserLoginField));
		/*
		CThostFtdcRspInfoField * t2 = new CThostFtdcRspInfoField;
		memset(t2, 0, sizeof(CThostFtdcRspInfoField));
		memcpy_s(t2, sizeof(CThostFtdcRspInfoField), p2, sizeof(CThostFtdcRspInfoField));
		*/
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			//delete t2;
			t1 = NULL;
			//t2 = NULL;
			printf("post message(MY_OnRspUserLogin) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspUserLogout:
	{
		CThostFtdcUserLogoutField * t1 = new CThostFtdcUserLogoutField;
		memset(t1, 0, sizeof(CThostFtdcUserLogoutField));
		memcpy_s(t1, sizeof(CThostFtdcUserLogoutField), p1, sizeof(CThostFtdcUserLogoutField));
		/*
		CThostFtdcRspInfoField * t2 = new CThostFtdcRspInfoField;
		memset(t2, 0, sizeof(CThostFtdcRspInfoField));
		memcpy_s(t2, sizeof(CThostFtdcRspInfoField), t2, sizeof(CThostFtdcRspInfoField));
		*/
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			//delete t2;
			t1 = NULL;
			//t2 = NULL;
			printf("post message(MY_OnRspUserLogin) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspQryInvestorPosition:
	{
		break;
	}
	case MY_OnRspQryTradingAccount:
	{
		break;
	}
	case MY_OnRtnOrder:
	{
		break;
	}
	case MY_OnRtnTrade:
	{
		break;
	}
	case MY_OnRtnDepthMarketData:
	{
		CThostFtdcDepthMarketDataField * t1 = new CThostFtdcDepthMarketDataField;
		memset(t1, 0, sizeof(CThostFtdcDepthMarketDataField));
		memcpy_s(t1, sizeof(CThostFtdcDepthMarketDataField), p1, sizeof(CThostFtdcDepthMarketDataField));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			t1 = NULL;
			printf("post message(MY_OnRtnDepthMarketData) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspSubMarketData:
	{
		CThostFtdcSpecificInstrumentField * t1 = new CThostFtdcSpecificInstrumentField;
		memset(t1, 0, sizeof(CThostFtdcSpecificInstrumentField));
		memcpy_s(t1, sizeof(CThostFtdcSpecificInstrumentField), p1, sizeof(CThostFtdcSpecificInstrumentField));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			t1 = NULL;
			printf("post message(MY_OnRspSubMarketData) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspUnSubMarketData:
	{
		CThostFtdcSpecificInstrumentField * t1 = new CThostFtdcSpecificInstrumentField;
		memset(t1, 0, sizeof(CThostFtdcSpecificInstrumentField));
		memcpy_s(t1, sizeof(CThostFtdcSpecificInstrumentField), p1, sizeof(CThostFtdcSpecificInstrumentField));
		if (!::PostThreadMessage(nThreadID, msg, (WPARAM)t1, NULL))
		{
			delete t1;
			t1 = NULL;
			printf("post message(MY_OnRspUnSubMarketData) failed, errno:%d\n", ::GetLastError());
		}
		break;
	}
	case MY_OnRspForQuote:
	{
		break;
	}
	case MY_OnRspAuthenticate:
	{
		break;
	}
	case MY_IsErrorRspInfo:
	{
		break;
	}
  }
}


CMdSpi::CMdSpi()
{
	 begintime1=-1;
	 begintime2=-1;
	 begintime3=-1;
	 begintime4=-1;

	 endtime1=-1;
	 endtime2=-1;
	 endtime3=-1;
	 endtime4=-1;
	mInitOK = FALSE;
	//mpUserApi = NULL;
	mhSyncObj = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	RequestID = 0;
}

CMdSpi::~CMdSpi()
{
	::CloseHandle(mhSyncObj);
}

void CMdSpi::OnRspError(CThostFtdcRspInfoField *pRspInfo,int nRequestID, bool bIsLast)
{
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	IsErrorRspInfo(pRspInfo);
}

void CMdSpi::OnFrontDisconnected(int nReason)
{
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	PMsg(nThreadID_OnFrontDisconnected, MY_OnFrontDisconnected, NULL, NULL, nReason);
	/*
	SYSTEMTIME t;
	::GetLocalTime(&t);
	std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
	std::cout << "--->>> " << __FUNCTION__ << std::endl;
	std::cout << "--->>> Reason = " << nReason << std::endl;
	*/
	::Beep(800, 500);
}

void CMdSpi::OnHeartBeatWarning(int nTimeLapse)
{
}

BOOL CMdSpi::Init()
{
	return TRUE;
}

void CMdSpi::OnFrontConnected()
{
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	PMsg(nThreadID_OnFrontConnected, MY_OnFrontConnected, NULL, NULL, 0);
    ///用户登录请求
	ReqUserLogin();	  
}

char *CMdSpi::GetApiVersion()
{
	_snprintf_s(ver,sizeof(ver),sizeof(ver),"%s", (char*)(mpUserApi->GetApiVersion()));
	return ver;
}

char *CMdSpi::GetTradingDay()
{
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	_snprintf_s(tradingday, sizeof(tradingday), sizeof(tradingday), "%s", (char*)(mpUserApi->GetTradingDay()));
	return tradingday;
}

void   CMdSpi::RegisterFront(char *pszFrontAddress)
{
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	mpUserApi->RegisterFront(pszFrontAddress);
}

void   CMdSpi::RegisterNameServer(char *pszNsAddress)
{
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	mpUserApi->RegisterNameServer(pszNsAddress);
}

int CMdSpi::ReqUserLogin()
{
	cerr << "--->>> " << __FUNCTION__ << std::endl;
	CThostFtdcReqUserLoginField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, gBrokerID.c_str());
	strcpy(req.UserID, gInvestorID.c_str());
	strcpy(req.Password, gPassword.c_str());
	printf("md login: %s,%s,%s \n",req.BrokerID, req.UserID, req.Password);

	return mpUserApi->ReqUserLogin(&req, ++RequestID);
}

int CMdSpi::ReqUserLogout()
{
	cerr << "--->>> " << __FUNCTION__ << std::endl;
 	CThostFtdcUserLogoutField req;
	memset(&req, 0, sizeof(req));
	strcpy(req.BrokerID, gBrokerID.c_str());
	strcpy(req.UserID, gInvestorID.c_str());
	//strcpy(req.Password, gPassword.c_str());
	return mpUserApi->ReqUserLogout(&req, ++RequestID);
}

void CMdSpi::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (!pRspUserLogin)
	{
		return;
	}
	PMsg(nThreadID_OnRspUserLogin, MY_OnRspUserLogin, pRspUserLogin, pRspInfo, nRequestID);

	return;
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		///获取当前交易日
		cerr << "TradeingDay: " << mpUserApi->GetTradingDay() << endl;
		mInitOK = TRUE;
	   if (pRspInfo && pRspInfo->ErrorID != 0)
	   {
			printf("OnRspUserLogin Failer,ErrorID=0x%04x, ErrMsg=%s\n", pRspInfo->ErrorID, pRspInfo->ErrorMsg);

			CThostFtdcRspUserLoginField tn;
			memset(&tn, 0, sizeof(CThostFtdcRspUserLoginField));
			memcpy_s(&tn, sizeof(CThostFtdcRspUserLoginField), pRspUserLogin, sizeof(CThostFtdcRspUserLoginField));
	   }
	   else
	   {
			printf("OnRspUserLogin Scuess\n");
			Sleep(3000);
	   }
	}
}

void CMdSpi::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (!pUserLogout)
	{
		return;
	}
	PMsg(nThreadID_OnRspUserLogout, MY_OnRspUserLogout, pUserLogout, 0, 0);
}

int CMdSpi::SubscribeForQuoteRsp(char * InstrumentID)
{
	return mpUserApi->SubscribeForQuoteRsp(&InstrumentID,1);
}

int ttid = 0;
int CMdSpi::SubscribeMarketData(char * InstrumentID)
{
    char Instrument_All_Name_Str[1][31] = {""};
    char * Instrument_All_Name[1];
	_snprintf_s(&Instrument_All_Name_Str[0][31], 31, 30, "%s", InstrumentID);
	Instrument_All_Name[0] = &Instrument_All_Name_Str[0][31];
	if (Instrument_All_Name[0])
	{
		int iResult = mpUserApi->SubscribeMarketData(&(Instrument_All_Name[0]), 1);
	}
}

int CMdSpi::SubscribeMarketData()
{
	return mpUserApi->SubscribeMarketData( &(ppInstrumentID[amount - 1]), 1);
}

int CMdSpi::UnSubscribeMarketData()
{
	if (amount >= 1)
	{
		int iResult = mpUserApi->UnSubscribeMarketData(&(ppInstrumentID[amount - 1]), 1);
		if (iResult != 0)
			cerr << "Failer:" << __FUNCTION__ << ppInstrumentID[amount - 1] << endl;
		else
			cerr << "Scuess:" << __FUNCTION__ << ppInstrumentID[amount - 1] << endl;
	}
	return 0;
}

bool firststate = true;

//加锁
HANDLE hThread[12];
unsigned int uiThreadId[12];
//加锁
/*
DWORD WINAPI strategy(const LPVOID lpParam)
//UINT strategy(LPVOID pParam)
// void CMdSpi::strategy()
{
	Py_Initialize();

	PyObject * pModule = NULL;
	PyObject * pFunc = NULL;
	pModule = PyImport_ImportModule("Test001");	    //Test001:Python文件名
	pFunc = PyObject_GetAttrString(pModule, "TestDict");	//Add:Python文件中的函数名
															//创建参数:
	PyObject *pArgs = PyTuple_New(1);
	PyObject *pDict = PyDict_New();   //创建字典类型变量
	PyDict_SetItemString(pDict, "Name", Py_BuildValue("s", "WangYao")); //往字典类型变量中填充数据
	PyDict_SetItemString(pDict, "Age", Py_BuildValue("i", 25));         //往字典类型变量中填充数据
	PyTuple_SetItem(pArgs, 0, pDict);//0---序号  将字典类型变量添加到参数元组中
									 //返回值
	PyObject *pReturn = NULL;
	pReturn = PyEval_CallObject(pFunc, pArgs);	    //调用函数
													//处理返回值:
	Py_ssize_t size = PyDict_Size(pReturn);
	cout << "返回字典的大小为: " << size << endl;
	PyObject *pNewAge = PyDict_GetItemString(pReturn, "Age");
	int newAge;
	PyArg_Parse(pNewAge, "i", &newAge);
	cout << "True Age: " << newAge << endl;

	Py_Finalize();
	return 1;
};

unsigned int __stdcall SniffAndTradingThread(void *lpParam)
{
	if (lpParam)
	{
		idrand* ir = (idrand*)lpParam;//获取参数结构体
		while (true)
		{
			showcalnum[ir->threadid]++;
			if (showcalnum[ir->threadid] > 200)
			{
				showcalnum[ir->threadid] = 0;
				printf("[%d]  %d~%d\n", ir->threadid, ir->beginid, ir->endid);
			}


			// WaitForSingleObject(semaphorehandle[ir->threadid], INFINITE);
			Sniffer(ir->beginid, ir->endid);
			Trading(ir->beginid, ir->endid);	//冲突

			::Sleep(100);
			//  ReleaseSemaphore(semaphorehandle[ir->threadid], 1, NULL);
		}
		delete lpParam;
		lpParam = NULL;
	}
	return 0;
}
*/

double GetLocalTimeSec2()
{
	SYSTEMTIME sys_time;
	GetLocalTime(&sys_time);
	double system_times;
	system_times = (double)((sys_time.wHour) / 10e1) + (double)((sys_time.wMinute) / 10e3) + (double)((sys_time.wSecond) / 10e5);	//格式时间0.145100
	return system_times;
}
void UpdatePriceHash(const char * InstrumentID, CThostFtdcDepthMarketDataField *pDepthMarketData)
{
	if (pDepthMarketData->LastPrice < 1)
	{
		return;
	}
	/*
	//时间
	MarketdataSource  * q = new MarketdataSource;
	q->key = 8;
	//memset(&r,0,sizeof(MarketdataSource));

	string temp = InstrumentID;
	//pair<string, MarketdataSource>value(temp, r);

	pair<string, MarketdataSource>value(temp, *q);
	QS_Data_Map::iterator it = gMarket.find(InstrumentID);

	if (it == gMarket.end())
	{
	//printf("*************没找到该合约的%s\n", InstrumentID);
	gMarket.insert(value);
	}
	else
	{
	printf("xxxxxxxxxxxxxxx找到该合约的%s\n", InstrumentID);
	//MarketdataSource  * r

	}
	q = &(it->second);
	printf("q->key : %d\n", q->key);
	*/
	//EnterCriticalSection(&g_csdata);
	//cmdlist.push_back(tn);
	//LeaveCriticalSection(&g_csdata);
	std::hash_map<string, MarketdataSource>::iterator it;
	it = gMarket.find(InstrumentID);
	if (it == gMarket.end())
	{	  // printf("*************没找到该合约的%s\n", InstrumentID);
		MarketdataSource value;
		//InitMarketdataSource(&value);
		gMarket.insert(std::make_pair(InstrumentID, value));
		//ClearMarketdataSource(&value);
		it = gMarket.find(InstrumentID);
	}
	if (it != gMarket.end())
	{
		//printf("xxxxxxxxxxxxxxx找到该合约的%s\n", InstrumentID);
		MarketdataSource * q = &(it->second);
		//it->second = value;
		//	it->second.D1_BS_BIG_BL_Value
		char names[10] = { 0 };
		char times[10] = { 0 };
	}
}

double z_min(double a, double b)
{
	if (a < 1e-7)
	{
		return b;
	}
	if (b < 1e-7)
	{
		return a;
	}
	return fmin(a,b);
}

int lastminutes = -1;
void CMdSpi::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData)
{	
	if (!pDepthMarketData){return;}

	PMsg(nThreadID_OnRtnDepthMarketData, MY_OnRtnDepthMarketData, pDepthMarketData, NULL, 0);

	return;
	char names[10] = { 0 };
	char times[10] = { 0 };
	strcpy_s(times, sizeof(times), (char*)(&pDepthMarketData->UpdateTime));
	string str0 = names;
	string str1 = times;
	string str2 = times;
	string str3 = times;
	str1 = str1.substr(0, 2);
	str2 = str2.substr(3, 2);
	str3 = str3.substr(6, 2);
	int  hours = atoi(str1.c_str());
	int  minutes = atoi(str2.c_str());
	int  seconds = atoi(str3.c_str());
	double thistime = 0.01* hours + 0.0001* minutes + 0.000001*seconds;

	std::hash_map<string, MarketdataSource>::iterator it = gMarket.find(pDepthMarketData->InstrumentID);
	if (it == gMarket.end())
	{
		//未找到该合约的
		MarketdataSource value;
		//InitMarketdataSource(&value);
		gMarket.insert(std::make_pair(pDepthMarketData->InstrumentID, value));
		it = gMarket.find(pDepthMarketData->InstrumentID);
	}
	ZQData tn;
	memset(&tn,0,sizeof(ZQData));
	if (minutes != lastminutes)
	{
		lastminutes = minutes;
		it->second.Data_M1.push_back(tn);
		(*(it->second.Data_M1.begin())).Open = pDepthMarketData->LastPrice;
		(*(it->second.Data_M1.begin())).High = pDepthMarketData->LastPrice;
		(*(it->second.Data_M1.begin())).Low = pDepthMarketData->LastPrice;
		(*(it->second.Data_M1.begin())).Close = pDepthMarketData->LastPrice;
		(*(it->second.Data_M1.begin())).AveragePrice = pDepthMarketData->AveragePrice;
		(*(it->second.Data_M1.begin())).HighestPrice = pDepthMarketData->HighestPrice;
		(*(it->second.Data_M1.begin())).LowestPrice = pDepthMarketData->LowestPrice;
		(*(it->second.Data_M1.begin())).ZQTime = thistime;
		_snprintf_s(&(*(it->second.Data_M1.begin())).TradeingDay, sizeof(TThostFtdcDateType), sizeof(TThostFtdcDateType)-1,"%s", pDepthMarketData->TradingDay);
	}
	else
	{
		(*(it->second.Data_M1.begin())).High =fmax((*(it->second.Data_M1.begin())).High, pDepthMarketData->LastPrice);
		(*(it->second.Data_M1.begin())).Low = z_min((*(it->second.Data_M1.begin())).Low, pDepthMarketData->LastPrice);
		(*(it->second.Data_M1.begin())).Close = pDepthMarketData->LastPrice;
	}
}

bool CMdSpi::IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo)
{
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = ((pRspInfo) && (pRspInfo->ErrorID != 0));
	if (bResult)
	{
		SYSTEMTIME t;
		::GetLocalTime(&t);
		std::cout << t.wHour << ":" << t.wMinute << ":" << t.wSecond << std::endl;
		std::cout << "--->>> ErrorID=" << pRspInfo->ErrorID << ", ErrorMsg=" << pRspInfo->ErrorMsg << std::endl;
		::Beep(800, 10000);

	}
	return bResult;
}

///订阅行情应答
void  CMdSpi::OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	PMsg(nThreadID_OnRtnDepthMarketData, MY_OnRspSubMarketData, pSpecificInstrument, NULL, 0);

};

///取消订阅行情应答
void  CMdSpi::OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	PMsg(nThreadID_OnRtnDepthMarketData, MY_OnRspUnSubMarketData, pSpecificInstrument, NULL, 0);
};

///询价通知
void  CMdSpi::OnRtnForQuoteRsp(CThostFtdcForQuoteRspField *pForQuoteRsp)
{

};